import React from "react";
import { BrowserRouter, Route, Switch } from "react-router-dom";
import Footer from "../../components/Footer";
import Header from "../../components/Header";
import DetailPage from "../DetailPage";
import HomePage from "../HomePage";
import "./mainapp.scss";

const MainApp = () => {
  return (
    <div className="main-app-wrapper">
      <Header />
      <div className="content-wrapper">
        <BrowserRouter>
          <Switch>
            <Route path="/detail/:id">
              <DetailPage />
            </Route>
            <Route path="/">
              <HomePage />
            </Route>
          </Switch>
        </BrowserRouter>
      </div>
      <Footer />
    </div>
  );
};

export default MainApp;
