import React, { useEffect, useState } from "react";
import { useHistory, withRouter } from "react-router-dom";
import LogoKucing from "../../assets/images/pawprint.png";
import Link from "../../components/Link";
import Gap from "../../components/Gap";
import "./detailpage.scss";
import axios from "axios";

function DetailPage(props) {
  const [data, setData] = useState({});

  useEffect(() => {
    const id = props.match.params.id;
    axios
      .get(`https://api.thecatapi.com/v1/breeds/${id}`)
      .then((res) => {
        // console.log("success", res.data);
        setData(res.data);
      })
      .catch((err) => {
        console.log("error", err);
      });
  }, [props]);

  const history = useHistory();
  return (
    <div className="detail-page-wrapper">
      <img className="img-cover" src={LogoKucing} alt="thumb" />
      <p className="list-title">{data.name}</p>
      <p className="list-country">{data.origin}</p>
      <ul>
        <li>Adaptability: {data.adaptability}</li>
        <li>Affection_level: {data.affection_level}</li>
        <li>Dog_friendly: {data.dog_friendly}</li>
      </ul>
      <p className="list-desc">Temperament: {data.temperament}</p>
      <p className="list-body">{data.description}</p>
      <Gap height={20} />
      <Link title="Back to Home" onClick={() => history.push("/")} />
    </div>
  );
}

export default withRouter(DetailPage);
