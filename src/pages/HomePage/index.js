import React, { useEffect, useState } from "react";
import ListItem from "../../components/ListItem";
import Button from "../../components/Button";
import Gap from "../../components/Gap";
import LogoKucing from "../../assets/images/pawprint.png";
import "./homepage.scss";
import axios from "axios";

function HomePage() {
  const [dataCat, setDataCat] = useState([]);
  const [page, setPage] = useState(0);

  const previous = () => {
    setPage(page <= 0 ? 0 : page - 1);
    // console.log(page);
  };

  const next = () => {
    setPage(page === 2 ? 2 : page + 1);
    // console.log(page);
  };
  useEffect(() => {
    axios
      .get(`https://api.thecatapi.com/v1/breeds?page=${page}&limit=10`)
      .then((res) => {
        // console.log("data API", res);
        const responseAPI = res.data;
        setDataCat(responseAPI);
      })
      .catch((err) => {
        console.log("error", err);
      });
  }, [page]);
  return (
    <div className="home-page-wrapper">
      <Gap height={20} />
      <div className="content-wrapper">
        {dataCat.map((cat) => {
          return (
            <ListItem
              key={cat.id}
              title={cat.name}
              country={cat.origin}
              image={cat.image.url}
              _id={cat.id}
              temperament={cat.temperament}
            />
          );
        })}
      </div>
      <div className="pagination">
        <Button title="Previous" onClick={previous} />
        <Gap width={20} />
        <Button title="Next" onClick={next} />
      </div>
      <Gap height={20} />
    </div>
  );
}

export default HomePage;

// ?page=0&limit=10
