import React from "react";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import DetailPage from "../../pages/DetailPage";
import MainApp from "../../pages/MainApp";

const Routes = () => {
  return (
    <div>
      <Router>
        <Switch>
          <Route path="/detail-page">
            <DetailPage />
          </Route>
          <Route path="/">
            <MainApp />
          </Route>
        </Switch>
      </Router>
    </div>
  );
};

export default Routes;
