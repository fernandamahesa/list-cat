import React from "react";
import LogoReact from "../../assets/icons/logo.svg";
import "./footer.scss";

function Footer() {
  return (
    <div>
      <div className="footer">
        <p className="power">Power by React</p>
        <img className="logo" src={LogoReact} alt="logo" />
      </div>
      <div className="copyright">
        <p>Copyright @2021 Fernanda Mahesa</p>
      </div>
    </div>
  );
}

export default Footer;
