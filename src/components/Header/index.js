import React from "react";
// import { useHistory } from "react-router-dom";
import "./header.scss";

function Header() {
  // const history = useHistory();
  return (
    <div className="header">
      <p className="logo-app">List-Cat</p>
    </div>
  );
}

export default Header;
