import React from "react";
import { useHistory } from "react-router-dom";
import Gap from "../Gap";
import LogoReact from "../../assets/icons/logo.svg";
import "./listitem.scss";
import Link from "../Link";

function ListItem({
  title,
  image,
  country,
  _id,
  affection_level,
  temperament,
}) {
  const history = useHistory();
  return (
    <div className="list-items">
      <img className="image-thumb" src={image} alt="post" />
      <div className="content-detail">
        <div className="title-wrapper">
          <p className="title">{title}</p>
        </div>
        <p className="country">{country}</p>
        <Gap height={10} />
        <p className="temperament">{temperament}</p>
        <Gap height={20} />
        <div className="link-view">
          <Link
            title="View Detail"
            onClick={() => history.push(`/detail/${_id}`)}
          />
        </div>
      </div>
    </div>
  );
}

export default ListItem;
